import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, ActionSheetController, ToastController } from 'ionic-angular';
import { User } from '../models/user';

import { GithubUsers } from '../../providers/github-users/github-users';

import { UserDetailsPage } from '../user-details/user-details';
import { AngularFireList } from 'angularfire2/database/';
import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase';
// import {Observable} from 'rxjs/Observable'
// import { Subject } from 'rxjs/Subject';
// import { DatabaseSnapshot } from '../../../node_modules/angularfire2/interfaces';
/**
 * Generated class for the UsersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-users',
  templateUrl: 'users.html',
})
export class UsersPage {
  users: User[]
  originalUsers: User[];
  users1: User[];
  //variable de lista  usuarios
  usuarios: AngularFireList<any>;

  constructor(public navCtrl: NavController, private githubUsers: GithubUsers, public database: AngularFireDatabase, public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController, private toastCtrl: ToastController) {
    //this.setUsers()
    this.users1 = [];
    this.database.list("/usuario/").subscribe(data => {
      this.users1 = [];
      firebase.database().ref("/usuario/").once("value", (snapshot) => {
        //let users1 = [];
        snapshot.forEach((snap) => {
          this.users1.push(snap.val());
          return false;
        });
      });
      
      this.users = this.users1;
      this.originalUsers = this.users;
      this.insertId();
    });

    githubUsers
      .searchUsers('scotch').subscribe(users => {
        console.log(users)
      });

  }


  setUsers() {
    this.githubUsers.load().subscribe(users => {
      this.users = users;
      this.originalUsers = users;

      firebase.database().ref('/usuario/').set(users);
    });
  }
  goToDetails(login: string) {
    this.navCtrl.push(UserDetailsPage, { login });
    //console.log('push')
  }
  search(ev) {
    // this.initializeItes();
    this.users1 = this.users;
    let SerVal = ev.target.value;

    if (SerVal && SerVal.trim() != '') {
      this.users = this.users.filter((mytasks1) => {
        return ((mytasks1.login).toLowerCase().indexOf(SerVal.toLowerCase()) > -1

        );
      });
    }
    if (SerVal.trim() === '' || SerVal.trim().length < 2) {

      this.users = this.originalUsers;
    }
  }
  alertExistente() {
    let prompt = this.alertCtrl.create({
      title: 'Error',
      message: "The name is not available",
      buttons:[
        {
          text:'OK',
          handler: data => {
            console.log('Cancel clicked');
          }
        }

      ] 
    });
    prompt.present();
  }


  addSong() {
    let prompt = this.alertCtrl.create({
      title: 'User Name',
      message: "Enter a name for the new user",
      inputs: [
        {
          name: 'name',
          //placeholder: 'Title'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: date => {
            //const newUser =this.users.push();


            var i = false;
            Object.keys(this.users).forEach(key => {
              var name= (this.users[key].login).toLowerCase()
              var name1= (date.name).toLowerCase()
              if ( name == name1  ) {
                i = true;
                debugger;
                this.alertExistente();
                
              }
            });
            if (i == false) {
              firebase.database().ref('/usuario/').push({ login: date.name, avatar_url: "https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png" })
              //this.insertId();
            }
          }
        }
      ]
    });

    prompt.present();
  }

  insertId(){
    firebase.database().ref('/usuario/').once('value', (snapshot) => {
      snapshot.forEach((snap) => {
        
          firebase.database().ref('/usuario/'+ snap.key +'/id_r').set(snap.key)
         return false;
      });
    })
  }
  //Eliminar
  remove(user) {
    debugger;
    console.log(user);
    let prompt = this.alertCtrl.create({
      title: 'What do you want to do?',
      buttons:
        [
          {

            text: 'Delete User',
            role: 'destructive',
            handler: date => {
            
              firebase.database().ref('/usuario/'+ user.id_r).remove().then(data => { this.navCtrl.setRoot(UsersPage); })
            
            }
          }, {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
    });
    prompt.present();
  }
 
}

